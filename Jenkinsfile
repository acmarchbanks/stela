/**
 * Configuration
 */
def projectName = 'postnl'



/**
 * Some script calls in Jenkins pipeline scripts are blocked from executing by default. This build file requires access
 * to some restricted classes. Rather than just displaying an exception message and forcing the administrator to find
 * the solution we accept a closure which is the call request and if it fails, we guide the user where to go to fix
 * the issue.
 */
def restrictedCall(Closure input){
	try {
		input()
	} catch(e){
		echo 'Restricted call: Go to "Manage Jenkins > In process script approval" and approve the restricted script calls.'
		throw e
	}
}



/**
 * The SCM URL is stored within the pipeline job. The URL is required for git when pushing updates back to origin.
 */
def scmUrl(){
	return restrictedCall {
		def url = scm.getUserRemoteConfigs()[0].getUrl()
		def uri = new URI(url)
		return uri.getHost() + uri.getPath()
	}
}



/**
 * Updates the latest commit with a new tag. If the tag already exists on the given commit then it will fail silently.
 */
def pushTag(tag, credentialsId){
	withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentialsId, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
		sh('git config --global user.name "$(whoami)"')
		sh('git config --global user.email "$(whoami)@$(hostname)"')
	
		try {
			sh("git tag -a " + tag + " -m 'Jenkins'")
			sh("git push https://${env.GIT_USERNAME}:${env.GIT_PASSWORD}@" + scmUrl() + " --tags")
		} catch(e){
			echo 'Tag already added'
		}
	}    
}



/**
 * Executes given params.task closure and if it fails, asks the user if it should be retried.
 * The task will then be executed again. If the user clicks abort, an exception will be
 * thrown aborting the pipeline.
 *
 * After the task either completes successfully or after the user clicks abort,
 * the params.andFinally closure will be executed.
 *
 * Usage:
 * <pre><code>
 * retry task: {
 *     sh 'something-flaky-generating-html-reports'
 * }, andFinally: {
 *     publishHTML target: [reportDir: 'report', reportFiles: 'index.html', reportName: 'Report']
 * }
 * </pre></code>
 *
 * @params params [task: {}, andFinally: {}]
 */
def retry (params) {
    // waitUntil will retry until the given closure returns true
    waitUntil {
        try {
            // execute the task, if this fails, an exception will be thrown
            // and the params.andFinally() wont be called
            (params.task)()
            (params.andFinally)()

            // make waitUntil stop retrying this closure
            return true
        }
        catch(e) {
            try {
                // input asks the user for "Retry? Proceed or Abort". If the
                // user clicks Proceed, input will just return normally.
                // If the user clicks Abort, an exception will be thrown
                input "Retry?"
            }
            catch (userClickedAbort) {
                // user clicked abort, call the andFinally closure and re-throw the exception
                // to actually abort the pipeline
                (params.andFinally)()
                throw userClickedAbort
            }

            // make waitUntil execute this closure again
            return false
        }
    }
}



/**
 * Begins the deploy process to migrate the Salesforce metadata to the next org.
 */
def invokeAntSalesforceDeploy(credentialsId, serverUrl, testLevel){

	// Invoke the Ant build.xml file
	withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentialsId, usernameVariable: 'SF_USERNAME', passwordVariable: 'SF_PASSWORD']]){
		withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {

			retry task: {
				sh 'ant -f ./build/deploy/build.xml -d deploy -Dsf.testLevel=' + testLevel + ' -Dsf.username=' + env.SF_USERNAME + ' -Dsf.org= -Dsf.password=' + env.SF_PASSWORD + ' -Dsf.token= -Dsf.serverurl=' + serverUrl + ' -q'
			}, andFinally: {
				echo 'User aborted the deployment.'
			}

		}
	}

}


/**
 * Executes the logic within a build stage. This allows us to send email notifications at the correct moments such
 * as when the build starts and finishes, or fails.
 */
def executeStage(Closure c){
	try {
		notifyBuild('STARTED')
		c();

	} catch (e) {
		// If there was an exception thrown, the build failed
		currentBuild.result = "FAILED"
		throw e
	} finally {
		// Success or failure, always send notifications
		notifyBuild(currentBuild.result)
	}
}


/**
 * Sends notifications regarding the status of the build stages.
 */
def notifyBuild(String buildStatus = 'STARTED') {
	// build status of null means successful
	buildStatus =  buildStatus ?: 'SUCCESSFUL'

	// Default values
	def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
	def summary = "${subject} (${env.BUILD_URL})"
	def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
	<p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

	emailext (
		subject: subject,
		body: details,
		recipientProviders: [[$class: 'DevelopersRecipientProvider']]
	)
}



/**
 * The pipeline stages.
 */
stage('TEST') {
	node {
		executeStage {
			checkout scm

			// Invoke the Ant build.xml file
			invokeAntSalesforceDeploy("${projectName}-org-test", 'https://login.salesforce.com', 'RunLocalTests')
			
			// Update the latest commit with the build number tag
			pushTag('TEST/' + env.BUILD_NUMBER, "${projectName}-repository-credentials")
		}
	}
}

stage('FAT') {
	node {
		// Invoke the Ant build.xml file
		invokeAntSalesforceDeploy("${projectName}-org-fat", 'https://login.salesforce.com', 'RunLocalTests')
		
		// Update the latest commit with the build number tag
		pushTag('FAT/' + env.BUILD_NUMBER, "${projectName}-repository-credentials")
	}
}

stage('UAT') {
	node {
		// Invoke the Ant build.xml file
		invokeAntSalesforceDeploy("${projectName}-org-uat", 'https://login.salesforce.com', 'RunLocalTests')
		
		// Update the latest commit with the build number tag
		pushTag('UAT/' + env.BUILD_NUMBER, "${projectName}-repository-credentials")
	}
}

stage('PRODUCTION') {
	input 'Ready to deploy?'
	node {
		// Invoke the Ant build.xml file
		invokeAntSalesforceDeploy("${projectName}-org-production", 'https://login.salesforce.com', 'RunLocalTests')
		
		// Update the latest commit with the build number tag
		pushTag('PRODUCTION/' + env.BUILD_NUMBER, "${projectName}-repository-credentials")
	}
} 