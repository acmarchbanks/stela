public class FacadePatternDemo {
   
   public static void runDemo(){
      ShapeMaker shapeMaker = new ShapeMaker();

      shapeMaker.drawCircle();
      shapeMaker.drawRectangle();
      shapeMaker.drawSquare();		
   }

}


// Output
// 
// Circle::draw()
// Rectangle::draw()
// Square::draw()