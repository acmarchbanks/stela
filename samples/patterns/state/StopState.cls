public class StopState implements State {

   public void doAction(Context context) {
      System.debug('Player is in stop state');
      context.setState(this);	
   }

   public String toString(){
      return 'Stop State';
   }

}