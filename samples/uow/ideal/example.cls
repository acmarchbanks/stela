UnitOfWork uow = new UnitOfWork();
Map<String, Order> orderByOrderID = new Map<String,Order>();

for(Order oOrder : orderList) {
	Order newOrder = new Order(
		OrderID__c=oOrder.orderID,
		OrderDate__c=oOrder.OrderDate__c
	);
	uow.registernew(oOrder); 
	orderByOrderID.put(oOrder.OrderID__c , oOrder); 
}

for(Products oProduct : productList){
	Product2 newProduct = new(Product2(ProductID__c=oProduct.ProductID);
	uow.registernew(newProduct, OrderId__c, orderByOrderID.get(oProduct.OrderID));
} 

uow.commitWork();
