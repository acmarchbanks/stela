List<Order> listOfOrders = new List<Order>();
Map<String,Order> orderByOrderID = new Map<String,Order>();

for(Order oOrder : orderList) {
	Order newOrder = new Order(
		OrderID__c= oOrder.orderID,
		OrderDate__c= oOrder.OrderDate__c
	);
	listOfOrders.add(newOrder);
}
			
insert listOfOrders;

for(Order oOrder : listOfOrders) {
	orderByOrderID.put(oOrder.OrderID__c , oOrder); 
}  

for(Products oProduct : productList) {
	Product2 newProduct = new Product2(
		ProductID__c= oProduct.ProductID, 
		OrderID__C= orderByOrderID.get(oProduct.OrderID).Id
	);
	listOfProducts.add(newProduct);
}

insert listOfProducts;
