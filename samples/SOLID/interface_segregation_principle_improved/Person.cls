public class Person implements DrinkingCapable, EatingCapable, SleepingCapable {
	
	public void doDrink(){
		System.debug('Action: Drink');
	}


	public void doEat(){
		System.debug('Action: Eat');
	}


	public void doSleep(){
		System.debug('Action: Sleep');
	}

}