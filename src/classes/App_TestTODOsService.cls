@isTest
private class App_TestTODOsService {
	
	@isTest
	private static void shouldMarkAllRecordAsCompletedCorrectly(){

		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		App_TODOsSelector.ISelector selectorMock = new App_Mocks.MockTODOsSelector(mocks);
		fflib_ISObjectUnitOfWork uowMock = new fflib_SObjectMocks.SObjectUnitOfWork(mocks);

		// Given
		mocks.startStubbing();
		
		List<TODO__c> records = new List<TODO__c>{
			new TODO__c()
		};

		mocks.when(selectorMock.sObjectType()).thenReturn(TODO__c.SObjectType);
		mocks.when(selectorMock.selectAll()).thenReturn(records);
		mocks.stopStubbing();

		// Set mocks
		App_Application.selector.setMock(selectorMock);
		App_Application.unitOfWork.setMock(uowMock);

		// When
		App_TODOsService.newInstance().markAllComplete();

		// Then
		((App_TODOsSelector.ISelector) mocks.verify(selectorMock)).selectAll();
		((fflib_ISObjectUnitOfWork) mocks.verify(uowMock, 1)).registerDirty(records);
		((fflib_ISObjectUnitOfWork) mocks.verify(uowMock, 1)).commitWork();

	}

}