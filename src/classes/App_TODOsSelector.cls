public with sharing class App_TODOsSelector extends fflib_SObjectSelector implements ISelector {

	public interface ISelector extends fflib_ISObjectSelector {
		List<TODO__c> selectAll();
	}

	public static ISelector newInstance(){
		return (ISelector) App_Application.selector.newInstance(TODO__c.SObjectType);
	}
	
	public List<Schema.SObjectField> getSObjectFieldList(){
		return new List<Schema.SObjectField> {
			TODO__c.Id,
			TODO__c.Subject__c,
			TODO__c.DueDate__c,
			TODO__c.Description__c,
			TODO__c.Complete__c
		};
	}

	public Schema.SObjectType getSObjectType(){
		return TODO__c.sObjectType;
	}

	public List<TODO__c> selectAll(){
		return (List<TODO__c>) Database.query( newQueryFactory().toSOQL() );
	}

}