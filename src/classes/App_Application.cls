public with sharing class App_Application {
	
	// Configure and create the UnitOfWorkFactory for this Application
	public static final fflib_Application.UnitOfWorkFactory unitOfWork = 
		new fflib_Application.UnitOfWorkFactory(
			new List<SObjectType> { 
				TODO__c.sObjectType
			}
		);

	// made a change	

	// Configure and create the ServiceFactory for this Application
	public static final fflib_Application.ServiceFactory service = 
		new fflib_Application.ServiceFactory( 
			new Map<Type, Type> {
				App_TODOsService.IService.class					=> App_TODOsService.class
			}
		);

	// Configure and create the SelectorFactory for this Application
	public static final fflib_Application.SelectorFactory selector = 
		new fflib_Application.SelectorFactory(
			new Map<SObjectType, Type> {
				TODO__c.sObjectType 							=> App_TODOsSelector.class
			}
		);

	// Configure and create the DomainFactory for this Application
	public static final fflib_Application.DomainFactory domain = 
		new fflib_Application.DomainFactory(
			App_Application.Selector,
			new Map<SObjectType, Type> {
				TODO__c.SObjectType 							=> App_TODOs.Constructor.class
			}
		);

}