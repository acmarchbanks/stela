public with sharing class App_TODOsController {
	
	private List<TODO__c> records;

	public App_TODOsController(){
		loadRecords();
	}


	private void loadRecords(){
		records = App_TODOsSelector.newInstance().selectAll();
	}


	public List<TODO__c> getRecords(){
		return records;
	}


	public PageReference addBlankRow(){
		records.add(
			new TODO__c()
		);
		return null;
	}


	public PageReference markAllComplete(){
		
		try {
			App_TODOsService.newInstance().markAllComplete();
		} catch(DMLEXception ex){
			ApexPages.addMessages(ex);
		}

		loadRecords();
		return null;
	}


	public PageReference updateRecords(){
		fflib_ISObjectUnitOfWork uow = App_Application.unitOfWork.newInstance();

		for(TODO__c todo : records){
			if(todo.Id == null){
				uow.registerNew(todo);
			} else {
				uow.registerDirty(todo);
			}
		}

		try {
			uow.commitWork();
		} catch(DMLEXception ex){
			ApexPages.addMessages(ex);
		}

		return null;
	}

}