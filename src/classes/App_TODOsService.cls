public with sharing class App_TODOsService implements IService {
	
	public interface IService {
		void markAllComplete();
	}


	public static IService newInstance(){
		return (IService) App_Application.service.newInstance(App_TODOsService.IService.class);
	}


	public void markAllComplete(){
		List<TODO__c> records = App_TODOsSelector.newInstance().selectAll();
		fflib_ISObjectUnitOfWork uow = App_Application.unitOfWork.newInstance();

		App_TODOs.IDomain domain = App_TODOs.newInstance(records);
		domain.markAllComplete();

		uow.registerDirty(records);
		uow.commitWork();
	}

}